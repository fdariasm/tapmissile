package coursera.nowxue.graphicstest;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.TextPaint;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
	private static final String MAX_KEY = "maxScore";
	private SharedPreferences prefs;
	private GraphicsView view = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button button = (Button) findViewById(R.id.button1);
		button.setOnClickListener(clickListener);
	}

	private View.OnClickListener clickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			prefs = getPreferences(MODE_PRIVATE);
			int maxScore = prefs.getInt(MAX_KEY, 0);
			view = new GraphicsView(MainActivity.this, maxScore);
			setContentView(view);
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	protected void onResume() {
		super.onResume();
		Log.d("Main", "onResume");
		if(null != view)view.start();
	}
	
	@Override
	protected void onPause() {
		Log.d("Main", "onPause");
		if(null != view)view.pause();
		super.onPause();
	}
	
	@Override
	protected void onStop() {
		if(null != view)prefs.edit().putInt(MAX_KEY, view.getMaxScore()).commit();
		super.onStop();
	}
}
class GraphicsView extends SurfaceView implements SurfaceHolder.Callback{
	private int CLINE;
	private int CIRCLESIZE;
	private int TARGETSIZE;
	private int OBSSIZE;
	private int SPEED;
	private float increment = 0.0f;
	private static final String TAG = GraphicsView.class.getSimpleName();
	private Thread secThread;
	private int width;
	private int height;
	private Paint mPaintStroke;
	private TextPaint tp = new TextPaint();
	private Object lock = new Object();
	private float x;
	private float y;
	private float angle = 0.0f;
	private float step= 0.073f;
	private float length = 0;
	private float target_angle;
	private int guide_x, guide_y;
	private int target_x, target_y;
	private boolean stop = true;
	private int score;
	private int maxScore;
	private List<Rect> obstacles = new LinkedList<Rect>();
	private Random random = new Random();
	private Paint mPaintFill;
	private Drawing drawing;
	private SurfaceHolder holder;
	public GraphicsView(Context context) {super(context);}
	public GraphicsView(Context context, int maxScore) {
		super(context);
		this.maxScore = maxScore;
		mPaintStroke = new Paint();
        mPaintStroke.setAntiAlias(true);
        mPaintStroke.setDither(true);
        mPaintStroke.setColor(0xFFFF0000);
        mPaintStroke.setStyle(Paint.Style.STROKE);
        mPaintStroke.setStrokeJoin(Paint.Join.ROUND);
        mPaintStroke.setStrokeCap(Paint.Cap.ROUND);
        mPaintStroke.setStrokeWidth(3);
        mPaintFill = new Paint();
        mPaintFill.setAntiAlias(true);
        mPaintFill.setDither(true);
        mPaintFill.setColor(Color.BLUE);
        mPaintFill.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaintFill.setStrokeJoin(Paint.Join.ROUND);
        mPaintFill.setStrokeCap(Paint.Cap.ROUND);
        mPaintFill.setStrokeWidth(3);
        Resources res = getResources();
        CIRCLESIZE = res.getInteger(R.integer.circle_size);
        CLINE = res.getInteger(R.integer.circle_line);
        TARGETSIZE = res.getInteger(R.integer.target_size);
        OBSSIZE = res.getInteger(R.integer.obstacle_size);
        SPEED = res.getInteger(R.integer.speed);
        score = 0;
		holder = getHolder();
        holder.addCallback(this);
	}
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		width = w;
		height = h;
		x = width/2;
		y = height/2;
		do {
			guide_x = (width + random.nextInt() % (2 * width)) / 4;
		} while (guide_x < 0 || guide_x > width);
		do {
			guide_y = (height + random.nextInt() % (2 * height)) / 4;
		} while (guide_y < 0 || guide_y > height);
		tp.setTextSize(height / 20);
		tp.setColor(Color.BLACK);
		tp.setFakeBoldText(true);
		createNewTarget();
		createObstacles();
	}
	private void createNewTarget() {
		do{
			do {
				target_x = (width + random.nextInt() % (2 * width)) / 4;
			} while (target_x < 0 || target_x > width);
			do {
				target_y = (height + random.nextInt() % (2 * height)) / 4;
			} while (target_y < 0 || target_y > height);
		}while(collideObstacle(target_x, target_y));
	}
	private void createObstacles() {
		obstacles.add(new Rect((int)x, 0, (int)x + OBSSIZE, height / 2 - OBSSIZE));
		obstacles.add(new Rect((int)x, height / 2 + 50, (int)x + OBSSIZE, height));
		int startx = width/15;
		int starty = height/8;
		obstacles.add(new Rect(startx, starty, width/2 - startx, height/8 + OBSSIZE));
		obstacles.add(new Rect(startx, height - starty, width/2 - startx, height - starty + OBSSIZE));
		startx = (width/2) + (startx + OBSSIZE);
		obstacles.add(new Rect(startx, starty, width -(width/15), height/8 + OBSSIZE));
		obstacles.add(new Rect(startx, height - starty, width - (width/15), height - starty + OBSSIZE));
		startx = width/4;
		starty = height/4;
		obstacles.add(new Rect(startx, starty, startx + OBSSIZE, height - (height/5)));
		obstacles.add(new Rect(width - startx, starty, width - startx + OBSSIZE, height - (height/5)));
	}
	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
	}
	public void pause(){
		boolean retry = true;
        drawing.setRunning(false);
        while (retry) {
            try {
                secThread.join();
                retry = false;
            } catch (InterruptedException e) {Log.e(TAG, "", e);}
        }
	}
	public void start(){
		drawing = new Drawing(holder);
        secThread = new Thread( drawing );
		drawing.setRunning(true);
		secThread.start();
	}
	@Override
	public void surfaceCreated(SurfaceHolder holder) {start();}
	public int getMaxScore(){return maxScore;}
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {pause();}
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		synchronized (lock) {
			switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
				guide_x = (int) event.getX();
				guide_y = (int) event.getY();
				stop = false;
				break;
			case MotionEvent.ACTION_MOVE:
				guide_x = (int) event.getX();
				guide_y = (int) event.getY();
				break;
			case MotionEvent.ACTION_UP: stop = true;}
		}
		return true;
	}
	class Drawing implements Runnable{
		private volatile boolean mRun = false;
		private SurfaceHolder mSurfaceHolder;
		public Drawing(SurfaceHolder surfaceHolder) {mSurfaceHolder = surfaceHolder;}
		
		public void setRunning(boolean run){mRun = run;}
		
		@Override
		public void run() {
			while(mRun){
				Canvas canvas = null;				
				try{
					canvas = mSurfaceHolder.lockCanvas();
					synchronized(mSurfaceHolder){doDraw(canvas);}
				}finally{
					if(null != canvas)mSurfaceHolder.unlockCanvasAndPost(canvas);
				}
			}
		}
	}
	public void doDraw(Canvas canvas) {
		if(null == canvas) return;
		canvas.drawColor(0xFFFFFFFF);
		synchronized (lock) {
		      x += length * Math.cos(angle);
		      y += length * Math.sin(angle);
		      float headx = (float)(x + CLINE * Math.cos(angle));
		      float heady = (float)(y + CLINE * Math.sin(angle));
		      length = (stopObject() || collideObstacle((int)headx, (int)heady)) 
		    		  ? 0 : SPEED + increment;		      
		      if(hitTarget(target_x, target_y)){
		    	  createNewTarget();
		    	  score += 10;
		    	  increment += 0.2;
		      }
		      updateScore(canvas);
		      canvas.drawCircle(x, y, CIRCLESIZE, mPaintStroke);
		      mPaintFill.setColor(Color.BLUE);
		      canvas.drawLine(x, y, headx, heady, mPaintFill);
		      canvas.drawCircle(target_x, target_y, TARGETSIZE, mPaintFill);
		      mPaintFill.setColor(Color.GREEN);
		      drawObstacles(canvas);
		      target_angle = (float) Math.atan2(guide_y-y,guide_x-x);
		      increaseAngle();
		}
	}
	private void updateScore(Canvas canvas) {
		canvas.drawText("Score: " + score, 10, height / 20 + 10, tp);
		if(score > maxScore) {maxScore = score;}
		canvas.drawText("Max: " + maxScore, width - (width / 7), height / 20 + 10, tp);
	}
	private void drawObstacles(Canvas canvas) {
		for(Rect rect : obstacles){canvas.drawRect(rect, mPaintFill);}
	}
	private boolean collideObstacle(int x, int y){
		for(Rect rect : obstacles){if(rect.contains(x, y))return true;}
		return false;
	}
	private boolean stopObject(){
		if(hitTarget(guide_x, guide_y))return true;	
		return stop;
	}
	private boolean hitTarget(int tar_x, int tar_y){return Math.abs(tar_x-x) + Math.abs(tar_y-y) < CIRCLESIZE;}
	private void increaseAngle() {
		float temp = (angle-target_angle);
	      if(temp > 2*Math.PI){temp -= 2 * Math.PI;}
	      else if(temp<0){temp += 2* Math.PI;}

	      if(temp < Math.PI){
	        angle-=step;
	        if(angle<0)angle+=(2 * Math.PI);
	      }
	      else{
	    	  angle+=step;
	    	  if(angle > 2 * Math.PI)
	    		  angle-=(2 * Math.PI);
	      }
	}
}
